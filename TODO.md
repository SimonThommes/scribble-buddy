### TODO

 [] geometry type selector
 [] add tools for different geometry types (Curve, Hair Curves, GPv3)
 [] figure out why datablock pickers don't work (material etc.)
 [] clear socket info on modifier deletion
 [] handle naming of new scribble data !
 [] link new scribble to contextual collection(s) !
 [x] preset material selector
 [x] link material slots from preset modifier context
 [] static vs deforming mode
 [x] style context modes (preset, scribble, auto)

 [x] base deformation modifier in resources
 [x] modifier selector for context scribble
 [x] modifier UI in tool settings as preset
 [] save as preset operator
 [] report about rest position
 [] report missing/insufficient UVs

 [] support multiple presets with names and make them shareable for the production
 [x] basic animation