import bpy

class SCRIB_link_context_setting(bpy.types.PropertyGroup):
    name: bpy.props.StringProperty(default='')
    link_context: bpy.props.BoolProperty(default=False)

class SCRIB_socket_info(bpy.types.PropertyGroup):
    name: bpy.props.StringProperty(default='')
    socket_info: bpy.props.CollectionProperty(type=SCRIB_link_context_setting)

class SCRIB_Settings(bpy.types.PropertyGroup):
    attach_to_active_selection: bpy.props.BoolProperty(default=True)
    preset_object: bpy.props.PointerProperty(type=bpy.types.Object, name="Preset Object")
    preset_material: bpy.props.PointerProperty(type=bpy.types.Material, name="Preset Material")
    assign_materials: bpy.props.BoolProperty(name='Assign Modifier Materials', default=True)
    style_context: bpy.props.EnumProperty(default='AUTO',
                                       items= [('PRESET', 'Preset', 'Specify the style of the current preset used for new scribbles', '', 0),\
                                               ('SCRIBBLE', 'Scribble', 'Specify the style of the currently active scribble', '', 1),
                                               ('AUTO', 'Auto', 'Specify the style of either the active scribble or the preset depending on the context', '', 2),
                                               ])
    
    try:
        gpv3 = bpy.context.preferences.experimental.use_grease_pencil_version3
    except:
        v0, v1, v3 = bpy.app.version
        gpv3 = v0 >= 4 and v1 >= 3
    curve_mode: bpy.props.EnumProperty(default='CURVES',
                                       items= [('CURVE', 'Legacy', 'Use legacy curve type (Limited Support)', 'CURVE_DATA', 0),\
                                               ('CURVES', 'Curves', 'Use hair curves (Fully supported)', 'CURVES_DATA', 1),
                                               ('GP', 'Grease Pencil', 'Use Grease Pencil (Limited Support)', 'OUTLINER_OB_GREASEPENCIL', 2),
                                               ] if gpv3 else 
                                               [('CURVE', 'Legacy', 'Use legacy curve type (Limited Support)', 'CURVE_DATA', 0),\
                                               ('CURVES', 'Curves', 'Use hair curves (Full Support)', 'CURVES_DATA', 1),
                                               ])
    #preset_mode

classes = [
    SCRIB_link_context_setting,
    SCRIB_socket_info,
    SCRIB_Settings,
]

def register():
    for c in classes:
        bpy.utils.register_class(c)
    bpy.types.Scene.SCRIB_settings = bpy.props.PointerProperty(type=SCRIB_Settings)
    bpy.types.Object.modifier_info = bpy.props.CollectionProperty(type=SCRIB_socket_info)

def unregister():
    for c in classes:
        bpy.utils.unregister_class(c)
    del bpy.types.Scene.SCRIB_settings
    del bpy.types.Object.modifier_info
