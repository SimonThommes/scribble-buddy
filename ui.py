import bpy
from . import utils

def draw_panel_ui_recursive(panel, panel_name, mod, items):

    scene = bpy.context.scene
    settings = scene.SCRIB_settings

    is_preset = mod.id_data == settings.preset_object and mod.id_data

    if not panel:
        return
    
    mod_info = mod.id_data.modifier_info.get(mod.name)
    
    link_icon_dict = {bpy.types.NodeTreeInterfaceSocketObject: 'OUTLINER_OB_SURFACE',
                      bpy.types.NodeTreeInterfaceSocketMaterial: 'MATERIAL'}

    for k, v in items:
        if type(v) == bpy.types.NodeTreeInterfacePanel:
            subpanel_header, subpanel = panel.panel(k, default_closed = v.default_closed)
            subpanel_header.label(text=k)
            draw_panel_ui_recursive(subpanel, k, mod, v.interface_items.items())
        else:
            if v.parent.name != panel_name:
                continue
            if f'{v.identifier}' not in mod.keys():
                continue
            row = panel.row()
            col = row.column()
            input_row = col.row(align=True)
            if f'{v.identifier}_use_attribute' in mod.keys():
                if mod[f'{v.identifier}_use_attribute']:
                    input_row.prop(mod, f'["{v.identifier}_attribute_name"]', text=k)
                else:
                    input_row.prop(mod, f'["{v.identifier}"]', text=k)
                if is_preset:
                    toggle = input_row.operator('scribble_buddy.preset_toggle_attribute', text='', depress=mod[f'{v.identifier}_use_attribute'], icon='SPREADSHEET')
                else:
                    toggle = input_row.operator('object.geometry_nodes_input_attribute_toggle', text='', depress=mod[f'{v.identifier}_use_attribute'], icon='SPREADSHEET')
                toggle.modifier_name = mod.name
                toggle.input_name = v.identifier
            else:
                input_row.prop(mod, f'["{v.identifier}"]', text=k)
            if not mod_info:
                continue
            if type(v) in utils.linkable_sockets:
                s = mod_info.socket_info.get(v.identifier)
                col.enabled = not s.link_context
                if not s:
                    continue
                icon = link_icon_dict[type(v)] if type(v) in link_icon_dict.keys() else 'LINKED'
                row.prop(s, 'link_context', text='', icon=icon)
    return

class SCRIB_PT_scribble_buddy_panel(bpy.types.Panel):
    bl_space_type = 'VIEW_3D'
    bl_region_type = 'UI'
    bl_label = "Scribble Buddy"
    bl_category = "Scribble Buddy"

    def draw(self, context):
        layout = self.layout

        settings = context.scene.SCRIB_settings

        surface_object = None
        if context.object:
            if 'SCRIB_surface_object' in context.object.keys():
                surface_object = context.object['SCRIB_surface_object']
            else:
                surface_object = context.object
            layout.label(text=f'Surface Object: {surface_object.name}' if surface_object else 'No Surface Object', icon='OUTLINER_OB_SURFACE')
        
        layout.prop(settings, 'curve_mode', expand=True)
        if settings.curve_mode in ['CURVE', 'GP']:
            layout.label(text='Curve mode does not support drawing on deformed geometry', icon='ERROR')
        layout.operator("scribble_buddy.new_scribble", icon='GREASEPENCIL')
        
        settings_header, settings_panel = layout.panel("scribble_settings", default_closed=True)
        settings_header.label(text="Settings")
        if settings_panel:
            settings_panel.prop(settings, 'assign_materials')
            row = settings_panel.row()
            row.prop(settings, 'style_context', expand=True)

        # identify style context
        style_object = context.object if settings.style_context=='SCRIBBLE' else settings.preset_object

        if context.object and settings.style_context=='AUTO':
            if 'SCRIB_surface_object' in context.object.keys():
                style_object = context.object

        is_preset = style_object == settings.preset_object

        style_header, style_panel = layout.panel("scribble_style", default_closed=False)
        if is_preset:
            style_header.label(text="Style (Preset)", icon='SETTINGS')
        else:
            style_header.label(text="Style (Scribble)", icon='OUTLINER_OB_GREASEPENCIL')
            style_header.operator('scribble_buddy.make_preset', text='', icon='DECORATE_OVERRIDE')

        if style_panel:
            style_panel.prop(settings, 'preset_material', icon='MATERIAL')
            if not settings.preset_object:
                style_panel.operator("scribble_buddy.init_preset", icon='MODIFIER')

            if style_object:
                for mod in style_object.modifiers:
                    mod_header, mod_panel = style_panel.panel(mod.name, default_closed = False)
                    mod_header.label(text='', icon='GEOMETRY_NODES')
                    mod_header.prop(mod, 'name', text='')

                    if is_preset:
                        op = mod_header.operator('scribble_buddy.preset_remove_mod', text='', icon='X')
                    else:
                        op = mod_header.operator('object.modifier_remove', text='', icon='X')
                    op.modifier = mod.name

                    if not mod_panel:
                        continue

                    if not mod.type == 'NODES':
                        mod_panel.label(text="Only 'Nodes' modifiers supported for preset interface")
                        continue

                    # show settings for nodes modifiers
                    mod_panel.prop(mod, 'node_group')
                    if not mod.node_group:
                        continue

                    draw_panel_ui_recursive(mod_panel, '', mod, mod.node_group.interface.items_tree.items())

            # expose add modifier operator for preset context
            if is_preset:
                style_panel.operator('scribble_buddy.preset_add_mod', icon='ADD')
            else:
                style_panel.operator('object.modifier_add', text='Add Modifier', icon='ADD')
        return

classes = [
    SCRIB_PT_scribble_buddy_panel,
]

def register():
    for c in classes:
        bpy.utils.register_class(c)

def unregister():
    for c in classes:
        bpy.utils.unregister_class(c)
