# Scribble Buddy

## Description

Scribble Buddy is a specialised tool for Blender to draw and attach curve data on top of mesh geometry in a way that it sticks to the surface regardless of deformation.