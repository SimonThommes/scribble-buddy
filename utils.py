import os
import bpy
from bpy.app.handlers import persistent

ng_list = [".scribble-buddy.processing", ".scribble-buddy.deformation"]

linkable_sockets = [bpy.types.NodeTreeInterfaceSocketObject, bpy.types.NodeTreeInterfaceSocketMaterial]

@persistent
def refresh_preset(dummy):
    settings = bpy.context.scene.SCRIB_settings
    if not settings:
        return
    if not settings.preset_object:
        return
    for mod in settings.preset_object.modifiers:
        if not mod.type == 'NODES':
            continue
        if not mod.node_group:
            continue
        mod_info = settings.preset_object.modifier_info.get(mod.name)
        if not mod_info:
            mod_info = settings.preset_object.modifier_info.add()
            mod_info.name = mod.name
        for v in mod.node_group.interface.items_tree.values():
            if not type(v) in linkable_sockets:
                continue
            if v.identifier in [s.name for s in mod_info.socket_info]:
                continue
            n = mod_info.socket_info.add()
            n.name = v.identifier
            # TODO: clean up old settings
    return

def get_addon_directory() -> str:
    """
    Returns the path of the addon directory to be used to append resource data-blocks.
    """
    return bpy.path.abspath(os.path.dirname(os.path.realpath(__file__)))

def import_resources(ng_names = ng_list):
    """
    Imports the necessary blend data resources required by the addon.
    """

    path = get_addon_directory()

    with bpy.data.libraries.load(f'{path}/scribble-buddy.resources.blend', link=False) as (data_src, data_dst):
        data_dst.node_groups = ng_names

def ensure_resources():
    ng_missing = set()
    
    for n in ng_list:
        if not bpy.data.node_groups.get(n):
            ng_missing.add(n)
    
    if ng_missing:
        import_resources(list(ng_missing))
    return

def transfer_modifier(modifier_name, target_obj, source_obj):
    """
    Core taken from https://projects.blender.org/studio/blender-studio-pipeline
    """

    # create target mod
    source_mod = source_obj.modifiers.get(modifier_name)
    target_mod = target_obj.modifiers.new(source_mod.name, source_mod.type)
    props = [p.identifier for p in source_mod.bl_rna.properties if not p.is_readonly]
    for prop in props:
        value = getattr(source_mod, prop)
        setattr(target_mod, prop, value)

    if source_mod.type == 'NODES':
        # Transfer geo node attributes
        for key, value in source_mod.items():
            target_mod[key] = value

        # Transfer geo node bake settings
        target_mod.bake_directory = source_mod.bake_directory
        for index, target_bake in enumerate(target_mod.bakes):
            source_bake = source_mod.bakes[index]
            props = [p.identifier for p in source_bake.bl_rna.properties if not p.is_readonly]
            for prop in props:
                value = getattr(source_bake, prop)
                setattr(target_bake, prop, value)


def register():
    bpy.app.handlers.depsgraph_update_post.append(refresh_preset)

def unregister():
    bpy.app.handlers.depsgraph_update_post.remove(refresh_preset)