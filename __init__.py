from . import utils, settings, ui, draw_tool, ops


bl_info = {
	"name": "Scribble Buddy",
	"author": "Simon Thommes",
    "version": (0, 1, 0),
	"blender": (4, 2, 0),
	"location": "3D Viewport > Sidebar > Scribble Buddy",
	"description": "Tool-set for the Blender Studio to easily draw curves on top of animated geometry.",
	"category": "Workflow",
}

modules = [utils, settings, ui, draw_tool, ops]

def register():
	for m in modules:
	    m.register()
    
def unregister():
	for m in modules:
	    m.unregister()