import bpy
from . import utils
from mathutils import Vector
from bpy.types import WorkSpaceTool

def node_group_settings(ng_settings, op_settings):
    
    ng_settings.nodes['Color'].value = [*op_settings.brush_color, 1.]
    ng_settings.nodes['Smear'].outputs[0].default_value = op_settings.brush_smear
    #ng_settings.nodes["Object Info"].inputs[0].default_value = surface_object
    
    return

class SCRIB_OT_draw(bpy.types.Operator):
    """
    Custom draw operation for hair curves
    """
    bl_idname = "scribble_buddy.draw"
    bl_label = "Custom Draw"
    bl_options = {'REGISTER', 'UNDO'}
    
    brush_color: bpy.props.FloatVectorProperty(name='Brush Color', size=3, subtype='COLOR', default=(.0,.0,.0), soft_min=0, soft_max=1, update=None)
    brush_smear: bpy.props.FloatProperty(name='Smear', subtype='FACTOR', default=0, min=0, max=1, update=None)

    ng_process = None
    
    init = True
    gp = None

    def modal(self, context, event):                
        if self.init:
            #bpy.ops.grease_pencil.brush_stroke('INVOKE_DEFAULT')
            bpy.ops.curves.draw('INVOKE_DEFAULT', wait_for_input=False)
            self.init = False
        else:
            self.ng_process.nodes['settings.color'].value = [*self.brush_color, 1.]
            self.ng_process.nodes['view_vector'].vector = context.space_data.region_3d.view_rotation @ Vector((0.0, 0.0, 1.0))
            self.ng_process.nodes['new_key'].boolean = context.scene.tool_settings.use_keyframe_insert_auto
            if 'SCRIB_surface_object' in context.object.keys():
                if context.object['SCRIB_surface_object']:
                    self.ng_process.nodes['surface_object'].inputs[0].default_value = context.object['SCRIB_surface_object']
            #node_group_settings(self.ng_settings, self)
            bpy.ops.geometry.execute_node_group(name="set_brush_stroke_color", session_uid=self.ng_process.session_uid)
            return {'FINISHED'}
        return {'RUNNING_MODAL'}


    def invoke(self, context, event):
        utils.ensure_resources()
        if True:
            SCRIB_OT_draw.gp = None
            SCRIB_OT_draw.ng_process = bpy.data.node_groups['.scribble-buddy.processing']

            context.tool_settings.curve_paint_settings.curve_type = 'POLY' # TODO: restore later
            context.tool_settings.curve_paint_settings.depth_mode = 'SURFACE' # TODO: restore later
            context.tool_settings.curve_paint_settings.use_pressure_radius = True # TODO: restore later

            context.window_manager.modal_handler_add(self)
            return {'RUNNING_MODAL'}
        else:
            self.report({'WARNING'}, "Not editable")
            return {'CANCELLED'}


class ScribbleCurves(WorkSpaceTool):
    bl_space_type = 'VIEW_3D'
    bl_context_mode = 'EDIT_CURVES'

    bl_idname = "scribble_buddy.draw"
    bl_label = "Scribble Draw"
    bl_description = (
        "Scribble on the visible surface"
    )
    bl_icon = "brush.gpencil_draw.draw"
    bl_widget = None
    bl_keymap = (
        ("scribble_buddy.draw", {"type": 'LEFTMOUSE', "value": 'CLICK_DRAG'},
         {"properties": [("wait_for_input", False)]}),
    )

    def draw_settings(context, layout, tool):
        props = tool.operator_properties("scribble_buddy.draw")
        layout.prop(context.tool_settings.curve_paint_settings , "radius_max")
        layout.prop(props, "brush_color")

classes = [
    SCRIB_OT_draw,
]

def register():
    for c in classes:
        bpy.utils.register_class(c)
    bpy.utils.register_tool(ScribbleCurves, after={"builtin.draw"}, group=True)

def unregister():
    for c in classes:
        bpy.utils.unregister_class(c)
    bpy.utils.unregister_tool(ScribbleCurves)