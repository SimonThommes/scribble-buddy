import bpy
from . import utils

class SCRIB_OT_new_scribble(bpy.types.Operator):
    """ Create new object according to type.
    Link to correct collection.
    Attach to selection context if applicable.
    Assign selected modifier setup. Enter correct context for editing.
    """
    bl_idname = "scribble_buddy.new_scribble"
    bl_label = "New Scribble"
    bl_description = "Create new scribble object of selected type with all the necessary setup in place"
    bl_options = {"REGISTER", "UNDO"}

    @classmethod
    def poll(cls, context):
        return context.mode == 'OBJECT'

    def execute(self, context):

        settings = context.scene.SCRIB_settings
        
        utils.ensure_resources()

        surface_object = context.object if context.object in context.selected_objects else None

        name = 'Scribble'

        if settings.curve_mode == 'GP':
            bpy.ops.object.grease_pencil_add(type='EMPTY')
            context.object.name = name
            context.object.data.name = name
            scribble_object = context.object
        else:
            if settings.curve_mode == 'CURVE':
                scribble_data = bpy.data.curves.new(name, type='CURVE')
                scribble_data.dimensions = '3D'
            elif settings.curve_mode == 'CURVES':
                scribble_data = bpy.data.hair_curves.new(name)
            scribble_object = bpy.data.objects.new(name, scribble_data)
            context.collection.objects.link(scribble_object)
            context.view_layer.objects.active = scribble_object
            for ob in bpy.data.objects:
                ob.select_set(False)
            scribble_object.select_set(True)

        # attach surface object pointer
        scribble_object['SCRIB_surface_object'] = surface_object

        if surface_object:
            surface_object.add_rest_position_attribute = True # TODO report if library data
            constraint = scribble_object.constraints.new('COPY_TRANSFORMS')
            constraint.target = surface_object
        
        # assign preset material
        if settings.preset_material:
            override = context.copy()
            override['object'] = scribble_object
            with context.temp_override(**override):
                bpy.ops.object.material_slot_add()
                scribble_object.material_slots[0].material = settings.preset_material
        
        if settings.preset_object:
            # transfer preset modifiers to new scribble
            for mod in settings.preset_object.modifiers:
                utils.transfer_modifier(mod.name, scribble_object, settings.preset_object)
                
                for v in mod.node_group.interface.items_tree.values():
                    if type(v) not in utils.linkable_sockets:
                        continue
                    if not settings.preset_object.modifier_info[mod.name].socket_info[v.identifier].link_context:
                        continue
                    if type(v) == bpy.types.NodeTreeInterfaceSocketObject:
                        scribble_object.modifiers[mod.name][f'{v.identifier}'] = surface_object
                    elif type(v) == bpy.types.NodeTreeInterfaceSocketMaterial:
                        scribble_object.modifiers[mod.name][f'{v.identifier}'] = settings.preset_material
            # refresh UI
            for mod in scribble_object.modifiers:
                mod.node_group.interface_update(context)
        else:
            # add deformation modifier
            mod = scribble_object.modifiers.new('Scribble Deformation', 'NODES')
            mod.node_group = bpy.data.node_groups['.scribble-buddy.deformation']
            mod['Socket_2'] = surface_object

        if settings.assign_materials:
            for mod in scribble_object.modifiers:
                for v in mod.node_group.interface.items_tree.values():
                    if type(v) != bpy.types.NodeTreeInterfaceSocketMaterial:
                        continue
                    mat = mod[v.identifier]
                    if not mat:
                        continue
                    if mat in [m_slot.material for m_slot in scribble_object.material_slots]:
                        continue
                    override = context.copy()
                    override['object'] = scribble_object
                    with context.temp_override(**override):
                        bpy.ops.object.material_slot_add()
                        scribble_object.material_slots[-1].material = mat

        
        for mod in scribble_object.modifiers:
            mod.show_group_selector = False

        # enter mode and tool context
            
        if settings.curve_mode == 'GP':
            bpy.ops.object.mode_set(mode='PAINT_GREASE_PENCIL')
            bpy.ops.wm.tool_set_by_id(name="builtin.draw")
            context.scene.tool_settings.gpencil_stroke_placement_view3d = 'SURFACE'
        else:
            bpy.ops.object.mode_set(mode='EDIT')
            bpy.ops.wm.tool_set_by_id(name="scribble_buddy.draw")

        return {"FINISHED"}
        
class SCRIB_OT_init_preset(bpy.types.Operator):
    """
    Initialize the preset to define a modifier stack applied to new scribbles.
    """
    bl_idname = "scribble_buddy.init_preset"
    bl_label = "Initialize Preset"
    bl_description = "Initialize the preset environment to setup a predefined modifier stack for new scribbles"
    bl_options = {"REGISTER", "UNDO"}

    @classmethod
    def poll(cls, context):
        settings = context.scene.SCRIB_settings
        return settings.preset_object is None

    def execute(self, context):

        settings = context.scene.SCRIB_settings
        
        utils.ensure_resources()
        preset_object = bpy.data.objects.new('SCRIB_PRESET', bpy.data.hair_curves.new('SCRIB_PRESET'))
        settings.preset_object = preset_object

        # add modifiers
        mod = preset_object.modifiers.new('Scribble Deformation', 'NODES')
        mod.node_group = bpy.data.node_groups['.scribble-buddy.deformation']
        
        mod_info = settings.preset_object.modifier_info.get(mod.name)
        if not mod_info:
            mod_info = settings.preset_object.modifier_info.add()
            mod_info.name = mod.name
        
        socket_info = mod_info.socket_info.get('Socket_2')
        if not socket_info:
            socket_info = mod_info.socket_info.add()
            socket_info.name = 'Socket_2'
        socket_info.link_context = True

        return {"FINISHED"}

class SCRIB_OT_make_preset(bpy.types.Operator):
    """
    Make the current scribble style specification the active preset.
    """
    bl_idname = "scribble_buddy.make_preset"
    bl_label = "Make Preset"
    bl_description = "Make the current scribble style specification the active preset"
    bl_options = {"REGISTER", "UNDO"}

    @classmethod
    def poll(cls, context):
        return 'SCRIB_surface_object' in context.object.keys()

    def execute(self, context):

        settings = context.scene.SCRIB_settings

        if not settings.preset_object:
            settings.preset_object = bpy.data.objects.new('SCRIB_PRESET', bpy.data.hair_curves.new('SCRIB_PRESET'))
        else:
            for mod in settings.preset_object.modifiers[:]:
                settings.preset_object.modifiers.remove(mod)
            
        # transfer scribble modifiers to preset
        for mod in context.object.modifiers:
            utils.transfer_modifier(mod.name, settings.preset_object, context.object)
        utils.refresh_preset(None)

        for mod in settings.preset_object.modifiers:
            # refresh UI
            mod.node_group.interface_update(context)
            
            # identify linked sockets
            for v in mod.node_group.interface.items_tree.values():
                if type(v) not in utils.linkable_sockets:
                    continue
                if not settings.preset_object.modifier_info[mod.name].socket_info[v.identifier]:
                    continue
                if type(v) == bpy.types.NodeTreeInterfaceSocketObject:
                    if context.object['SCRIB_surface_object']==mod[v.identifier]:
                        mod[v.identifier] = None
                        settings.preset_object.modifier_info[mod.name].socket_info[v.identifier].link_context = True
                elif type(v) == bpy.types.NodeTreeInterfaceSocketMaterial:
                    pass # TODO: figure out material preset linking
        return {"FINISHED"}
class SCRIB_OT_preset_add_mod(bpy.types.Operator):
    """
    Add a modifier to the preset stack. 
    """
    bl_idname = "scribble_buddy.preset_add_mod"
    bl_label = "Add Preset Modifier"
    bl_description = "Add a modifier to the preset"
    bl_options = {"REGISTER", "UNDO"}

    @classmethod
    def poll(cls, context):
        settings = context.scene.SCRIB_settings
        return settings.preset_object

    def execute(self, context):

        settings = context.scene.SCRIB_settings
        settings.preset_object.modifiers.new('Scribble Style', type='NODES')

        return {"FINISHED"}

class SCRIB_OT_preset_remove_mod(bpy.types.Operator):
    """
    Remove a modifier from the preset stack. 
    """
    bl_idname = "scribble_buddy.preset_remove_mod"
    bl_label = "Remove Preset Modifier"
    bl_description = "Remove a modifier from the preset"
    bl_options = {"REGISTER", "UNDO"}

    modifier: bpy.props.StringProperty(default='')

    @classmethod
    def poll(cls, context):
        settings = context.scene.SCRIB_settings
        return settings.preset_object

    def execute(self, context):

        settings = context.scene.SCRIB_settings
        with context.temp_override(object=settings.preset_object):
            bpy.ops.object.modifier_remove(modifier=self.modifier)

        return {"FINISHED"}

class SCRIB_OT_preset_toggle_attribute(bpy.types.Operator):
    """
    Toggle use_attribute property for a socket on a specific object's modifier. (Workaround due to how these are actually stored as integer in Blender)
    """
    bl_idname = "scribble_buddy.preset_toggle_attribute"
    bl_label = "Toggle Attribute"
    bl_description = "Toggle using a named attribute for this input"
    bl_options = {"REGISTER", "UNDO"}

    modifier_name: bpy.props.StringProperty(default='GeometryNodes')
    input_name: bpy.props.StringProperty(default='Socket_2')

    @classmethod
    def poll(cls, context):
        settings = context.scene.SCRIB_settings
        return settings.preset_object

    def execute(self, context):
        settings = context.scene.SCRIB_settings
        override = context.copy()
        override['object'] = settings.preset_object
        with context.temp_override(**override):
            bpy.ops.object.geometry_nodes_input_attribute_toggle(input_name=self.input_name, modifier_name=self.modifier_name)
        return {"FINISHED"}

classes = [
    SCRIB_OT_new_scribble,
    SCRIB_OT_init_preset,
    SCRIB_OT_make_preset,
    SCRIB_OT_preset_add_mod,
    SCRIB_OT_preset_remove_mod,
    SCRIB_OT_preset_toggle_attribute,
    ]

def register():
    for c in classes:
        bpy.utils.register_class(c)

def unregister():
    for c in classes:
        bpy.utils.unregister_class(c)